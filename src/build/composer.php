<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\cd;
use function Deployer\desc;
use function Deployer\get;
use function Deployer\task;
use Symfony\Component\Process\Process;

desc('Runs composer install --no-dev in "working_path". Requires to get executed after build:prepare.');
task('build:composer', function() {
    cd('');

    $composerCall = get('bin/composer') . ' ' . get('composer_options');
    writeAndLog('Performing "' . $composerCall . '"... ');

    $process = new Process($composerCall, get('build_path'), null, null, null);
    $process->run();
    if (!$process->isSuccessful()) {
        throw new \RuntimeException('Error occured when executing composer:' . PHP_EOL . $process->getErrorOutput());
    }
    writeln('done.');
})->once();
