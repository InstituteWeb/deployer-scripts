<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\cd;
use function Deployer\desc;
use function Deployer\get;
use function Deployer\input;
use function Deployer\logger;
use function Deployer\task;
use Symfony\Component\Process\Process;

desc('Get code from given git credentials and branch.');
task('build:update_code', function() {
    cd('');

    $buildPathAlreadyStaged = true;
    $process = new Process(get('bin/git') . ' status', get('build_path'));
    $process->run();
    if (!$process->isSuccessful()) {
        $buildPathAlreadyStaged = false;
    }

    if ($buildPathAlreadyStaged) {
        throw new \RuntimeException(
            'Given build path "' . get('build_path') . '" is already under git versioning. Can\'t proceed with cloning git repository.'
        );
    }

    writeAndLog('Cloning "' . get('git_repository') . '" and checking out "' . get('git_target') . '"... ');

    // Build git checkout command, and check for HOME environment variable
    $env = [];
    if (getenv('HOME')) {
        $env['HOME'] = getenv('HOME');
    }


    $branch = '--branch="' . get('git_target') . '"';
    $depth = '--depth="1"';
    if(get('git_target_is_revision')) {
        $branch = '';
        $depth = '';
    }

    // Start cloning
    $process = new Process(
        get('bin/git') . ' clone ' . $depth . ' ' . $branch . ' "' . get('git_repository') . '" "."',
        get('build_path'),
        $env
    );
    $process->inheritEnvironmentVariables();
    $process->run();
    if (!$process->isSuccessful()) {
        writeln('ERROR.');
        throw new \RuntimeException('Error while cloning: ' . $process->getErrorOutput());
    }
    logger('Cloning done.');
    writeln('done.');

    if(get('git_target_is_revision')) {
        $short = substr(get('revision'), 0, 7);
        writeAndLog('Git revision given. Resetting hard to "' . $short . '"... ');
        $process = new Process(get('bin/git') . ' reset ' . $short . ' --hard', get('build_path'));
        $process->run();
        if (!$process->isSuccessful()) {
            writeln('ERROR.');
            throw new \RuntimeException('Error while resetting: ' . $process->getErrorOutput());
        }
        writeln('done.');
    }

    $process = new Process(get('bin/git') . ' rev-parse HEAD', get('build_path'));
    $process->run();

    if ($process->isSuccessful()) {
        $revision = trim($process->getOutput());
        $shortRevision = substr($revision, 0, 7);
        input()->setOption('revision', $shortRevision);
        writelnAndLog('Git revision is: ' . $shortRevision . ' (' . $revision . ')', \Monolog\Logger::INFO);
    } else {
        writelnAndLog('Error while get revision from git: ' . $process->getErrorOutput(), \Monolog\Logger::ERROR);
    }
})->once();
