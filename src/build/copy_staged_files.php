<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\cd;
use function Deployer\desc;
use function Deployer\get;
use function Deployer\logger;
use function Deployer\task;
use Symfony\Component\Process\Process;

desc('Copies all staged files to "working_path". Requires to get executed after build:prepare.');
task('build:copy_staged_files', function() {
    cd('');

    writeAndLog('Get all files in current project, staged with git... ');

    // Get list of all staged files in Git
    $process = new Process('git ls-files');
    $process->run();
    if (!$process->isSuccessful()) {
        throw new \RuntimeException(
            'The command <info>git ls-files</info> failed on cli:' . PHP_EOL . $process->getErrorOutput()
        );
    }

    // Get files and split them by new line
    $files = explode("\n", trim($process->getOutput()));
    if (!is_array($files) || empty($files)) {
        throw new \RuntimeException('No staged files found.');
    }
    writeln('done.');

    // Copy all staged files to .build directory
    writeAndLog('Copying ' . count($files) . ' staged files from local git to "' . get('build_path')  . '" directory... ');
    foreach ($files as $file) {
        $destinationPath = get('build_path') . '/' . $file;
        if (!file_exists($file)) {
            throw new \RuntimeException(
                'The file "' . $file . '" was not found. Please make sure you have changes in git left, when building project!'
            );
        }
        logger('Copy file "' . $file . '" to "' . $destinationPath . '"');
        filesLocal()->copy($files, $destinationPath);
    }
    logger('Copy done.');
    writeln('done.');
})->once();
