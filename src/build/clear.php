<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\cd;
use function Deployer\desc;
use function Deployer\get;
use function Deployer\logger;
use function Deployer\task;

desc('Removes build folder (but not archive file). Do not store the zip file within your build directory if you run this task!');
task('build:clear', function() {
    cd('');
    if (!is_dir(get('build_path'))) {
        throw new \RuntimeException('Build path "' . get('build_path') . '" not found. Please check {{build_path}} option.');
    }

    writeAndLog('Removing build directory "' . get('build_path') . '"... ');
    sleep(0.5);
    filesLocal()->deleteDir(get('build_path'));
    writeln('done.');
    logger('Directory successfully removed.');
})->once();
