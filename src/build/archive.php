<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\cd;
use function Deployer\desc;
use function Deployer\get;
use function Deployer\logger;
use function Deployer\parse;
use function Deployer\task;
use Symfony\Component\Finder\Finder;

desc('Create zip archive of whole build content.');
task('build:archive', function() {
    cd('');

    $archiveList = get('archive_list');
    if (!$archiveList || empty($archiveList) || !is_array($archiveList) ) {
        throw new \RuntimeException('Given "archive_list" is invalid. Array with key (source directory) and values (zip file path) expected.');
    }
    foreach ($archiveList as $sourceDirectoryPath => $zipPath) {
        $sourceDirectoryPath = parse($sourceDirectoryPath);
        $zipPath = parse($zipPath);
        writeln('Create "' . $zipPath . '"');
        writeAndLog('- collecting files... ');
        $files = (new Finder())->files()
            ->in($sourceDirectoryPath)
            ->ignoreDotFiles(false)
            ->ignoreVCS(true);
        writeln('done.');

        // Prepare zip file
        $actionName = '- create';
        if (file_exists($zipPath)) {
            $actionName = '- overwrite existing';
        }
        writeAndLog($actionName . ' zip archive "' . $zipPath . '" with ' . count($files) . ' files... ');

        // Create zip file
        $zip = new \ZipArchive();
        if ($zip->open($zipPath, \ZipArchive::CREATE) !== true) {
            throw new \RuntimeException('Can\'t create zip archive "' . $zipPath . '"');
        }
        foreach ($files as $file) {
            $relativePath = str_replace('\\', '/', $file->getRelativePath());
            $filename = $file->getFilename();
            if ($file->getRelativePath() !== '') {
                $filename = '/' . $filename;
            }
            $zip->addFile($file->getRealPath(), $relativePath . $filename);
        }
        $zip->close();

        if (file_exists($zipPath)) {
            writeln('done.');
            logger('Zip archive successfully created.');
        } else {
            throw new \RuntimeException('Zip archive could not get created.');
        }
    }
})->once();
