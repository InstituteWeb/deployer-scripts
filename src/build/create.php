<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\cd;
use function Deployer\desc;
use function Deployer\get;
use function Deployer\logger;
use function Deployer\task;

desc('Creates build directory in configured build directory (build_dir).');
task('build:create', function() {
    cd('');

    if (filesLocal()->has(get('build_path'))) {
        write('Folder "' . get('build_path') . '" already existing. Deleting it... ');
        logger('Removing existing folder: ' . get('build_path'));
        $status = filesLocal()->delete(get('build_path'));
        if (!$status) {
            throw new \RuntimeException('Unable to remove directory "' . get('build_path') . '"');
        }
        writeln('done.');
    }

    filesLocal()->createDir(get('build_path'));

    writelnAndLog('Created folder "' . get('build_path') . '"');
})->once();

