<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\argument;
use function Deployer\desc;
use function Deployer\get;
use function Deployer\input;
use function Deployer\option;
use function Deployer\set;
use function Deployer\task;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

require (__DIR__ . '/functions.php');

require (__DIR__ . '/build.php');
require (__DIR__ . '/build/create.php');
require (__DIR__ . '/build/copy_staged_files.php');
require (__DIR__ . '/build/update_code.php');
require (__DIR__ . '/build/composer.php');
require (__DIR__ . '/build/archive.php');
require (__DIR__ . '/build/clear.php');

require (__DIR__ . '/deploy.php');
require (__DIR__ . '/deploy/prepare.php');
require (__DIR__ . '/deploy/lock.php');
require (__DIR__ . '/deploy/release.php');
require (__DIR__ . '/deploy/upload.php');
require (__DIR__ . '/deploy/composer.php');
require (__DIR__ . '/deploy/shared.php');
require (__DIR__ . '/deploy/rollout.php');
require (__DIR__ . '/deploy/unlock.php');
require (__DIR__ . '/deploy/rollback.php');

require (__DIR__ . '/deploy/typo3/run_post_update_cmd.php');

// Define arguments and options
argument('stage', InputArgument::OPTIONAL, 'Run tasks only on this server or group of servers');
option('revision', null, InputOption::VALUE_OPTIONAL, 'Revision to deploy.');
option('force', null, InputOption::VALUE_NONE, 'Ignores .lock file on remote');
option('tag', null, InputOption::VALUE_OPTIONAL, 'Tag to deploy.');
option('branch', null, InputOption::VALUE_OPTIONAL, 'Branch to deploy.');
option('name', null, InputOption::VALUE_OPTIONAL, 'Optional build name. If empty, the branch, tag or revision name is taken.');

// Initial Deployer Configuration Parameters

// Used binaries
set('bin/php', get('bin/php') ?: getenv('PHP_BIN') ?: 'php');
set('bin/git', get('bin/git') ?: getenv('GIT_BIN') ?: 'git');
set('bin/composer', get('bin/composer') ?: getenv('COMPOSER_BIN') ?: 'composer');

// File operations (unix)
set('bin/unzip', 'unzip -qo "%s" -d "%s"');
set('bin/rm', 'rm -rf %s');
set('bin/cp', 'cp -rf "%s" "%s"');
set('bin/symlink', 'ln -nfs %s %s');
set('bin/symlink-file', get('bin/symlink'));
set('directory_permissions', 0755);

//// File operations (windows)
//set('bin/unzip', 'unzip -qo "%s" -d "%s"');
//set('bin/rm', 'del /S /Q "%s"'); // windows
//set('bin/cp', 'copy /Y /L "%s" "%s"'); // windows
//set('bin/symlink', 'mklink /D %2$s %1$s'); // windows
//set('bin/symlink-file', 'mklink %2$s %1$s'); // windows


// Build settings
set('build_dir', '../.builds'); // absolute path required
set('build_directory', function() {
    $realBuildDir = realpath(getcwd() . DIRECTORY_SEPARATOR . ltrim(get('build_dir'), '\\/'));
    if (!$realBuildDir) {
        throw new \RuntimeException('Do not find build_dir "' . get('build_dir') . '"');
    }
    return $realBuildDir;
});
set('build_path', function() {
    return get('build_directory') . DIRECTORY_SEPARATOR . get('build_name');
});
set('zip_file', '{{short_revision}}.zip');
set('archive_list', [
    '{{build_path}}' => '{{build_directory}}/{{zip_file}}'
]);

// Git
set('git_repository', null);
set('git_branch', 'master');

// Composer setup
set('composer_action', 'install');
set('composer_options', '{{composer_action}} --prefer-dist --no-progress --no-suggest --no-interaction --no-dev --optimize-autoloader');

// Deployment
set('release_name', '{{build_name}}');
set('release_path', '{{deploy_path}}/releases/{{release_name}}'); // current release path
set('max_release_cycle', 5);
set('use_symlink_for_current', true);
set('upload_list', [
    '{{build_directory}}/{{zip_file}}' => '{{deploy_path}}/releases/{{release_name}}.zip'
]);

// Combining tasks (validate, build and deploy)
desc('Builds project locally and ');
task('build-deploy', [
    'build',
    'deploy'
]);

// Dynamic configuration parameters

set('git_target_is_revision', null);
set('git_target', function() {
    $gitTarget = get('git_branch'); // master branch is default
    if (input()->hasOption('branch')) {
        $inputBranch = input()->getOption('branch');
        if (!empty($inputBranch)) {
            $gitTarget = $inputBranch;
        }
    }
    if (input()->hasOption('tag')) {
        $tag = input()->getOption('tag');
        if (!empty($tag)) {
            $gitTarget = $tag;
        }
    }
    if (input()->hasOption('revision') && !empty(input()->getOption('revision'))) {
        $gitTarget = input()->getOption('revision');
        set('git_target_is_revision', true);
    }
    if (input()->hasArgument('revision') && !empty(input()->getArgument('revision'))) {
        $gitTarget = input()->getArgument('revision');
        set('git_target_is_revision', true);
    }
    return $gitTarget;
});

set('revision', function() {
    if (input()->hasOption('revision') && !empty(input()->getOption('revision'))) {
        return input()->getOption('revision');
    }
    if (input()->hasArgument('revision') && !empty(input()->getArgument('revision'))) {
        return input()->getArgument('revision');
    }
    return null;
});
set('short_revision', function() {
    return substr(get('revision'), 0, 7);
});

set('build_name', function() {
    if (input()->hasOption('name') && !empty(input()->getOption('name'))) {
        $name = input()->getOption('name');
    } else {
        $name = get('git_target');
        if (get('git_target_is_revision') === true) {
            $name = substr($name, 0, 7);
        }
    }
    return str_replace(' ', '_', $name);
});

