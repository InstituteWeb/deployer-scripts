<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Class Filesystem
 *
 * @package InstituteWeb\DeployerScripts
 */
class Filesystem extends \League\Flysystem\Filesystem
{
    /**
     * Runs command on remote shell
     *
     * @param string $cmd
     * @return mixed
     */
    public function run($cmd)
    {
        if (method_exists($this->getAdapter(), 'run')) {
            return $this->getAdapter()->run($cmd);
        }
        return false;
    }
}
