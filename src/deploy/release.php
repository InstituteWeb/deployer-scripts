<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\cd;
use function Deployer\desc;
use function Deployer\get;
use function Deployer\logger;
use function Deployer\set;
use function Deployer\task;

desc('Register release.');
task('deploy:release', function() {
    cd('');

    $releasesJsonFilePath = get('deploy_path') . '/.dep/releases.json';
    $releases = json_decode(files()->read($releasesJsonFilePath), true) ?: [];
    $ids = array_keys($releases);
    $newReleaseIndex = intval(end($ids)) + 1;

    // Remove old releases
    if (get('max_release_cycle') > 0 && count($releases) >= get('max_release_cycle')) {
        writelnAndLog('Max release cycle count (' . get('max_release_cycle') . ') reached. Removing old releases.', \Monolog\Logger::INFO);

        $deleteAmount = count($releases) - get('max_release_cycle') + 1;
        for ($i = 1; $i <= $deleteAmount; $i++) {
            $firstReleaseItem = array_shift($releases);
            writeln('- removed old release "' . $firstReleaseItem['name'] . '" from releases.json');

            if (isset($firstReleaseItem['path']) && files()->has($firstReleaseItem['path'])) {
                write('- removing directory "' . $firstReleaseItem['path'] . '"... ');
                files()->deleteDir($firstReleaseItem['path']);
                writeln('done.');
                logger('Removed directory: ' . $firstReleaseItem['path']);
            } else {
                writeln('- no directory "' . $firstReleaseItem['path'] . '" found');
            }
        }
    }

    // Check if new release is already existing and append _123 to duplicates
    foreach ($releases as $release) {
        if ($release['path'] == get('release_path') && files()->has(get('release_path'))) {
            writelnAndLog('Release path "' . get('release_path') . '" already existing.', \Monolog\Logger::WARNING);
            set('release_path', get('release_path') . '_' . $newReleaseIndex);
            set('release_name', get('release_name') . '_' . $newReleaseIndex);
            writelnAndLog('Renaming release name and path to: ' . get('release_name') . ' (' . get('release_path') . ')');
            break;
        }
    }

    // Add new entry to releases.json file
    $releases[$newReleaseIndex] = [
        'name' => get('release_name'),
        'path' => get('release_path'),
        'time' => date('r'),
        'time_unix' => time(),
    ];
    files()->write($releasesJsonFilePath, json_encode($releases, JSON_PRETTY_PRINT));
    writelnAndLog('Added new release "' . get('release_name') . '" to ' . $releasesJsonFilePath);
});
