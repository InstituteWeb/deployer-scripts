<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\cd;
use function Deployer\desc;
use function Deployer\get;
use function Deployer\logger;
use function Deployer\run;
use function Deployer\task;

desc('Creates current directory (or symlink) which contains the current released version of your project.');
task('deploy:rollout', function() {
    cd('');

    writeln('Rolling out release "' . get('release_name') . '"...');

    if (get('use_symlink_for_current') !== false) {
        // Symlink shared file in release
        run(vsprintf(get('bin/symlink-file'), [get('release_path'), get('deploy_path') . '/current']));
        writelnAndLog('Created symlink "' . get('deploy_path') . '/current' . '" pointing to "' . get('release_path') . '"');
        return;
    }

    // Copy the release_path to a temporary current folder and then replace /current with temporary one
    writelnAndLog('"use_symlink_for_current" option has been disabled for this remote.', \Monolog\Logger::INFO);

    write('Copying release to temp directory... ');
    $tempCurrentFolder = get('deploy_path') . '/current' . uniqid('_temp_');
    files()->copy(get('release_path'), $tempCurrentFolder);
    writeln('done.');
    logger('Release successfully copied from "' . get('release_path') . '" to "' . $tempCurrentFolder . '"');

    writeAndLog('Switch current_temp to current... ');
    $tempOldCurrentFolder = false;
    if (files()->has(get('deploy_path') . '/current')) {
        $tempOldCurrentFolder = get('deploy_path') . '/current' . uniqid('_old_');
        files()->rename(get('deploy_path') . '/current', $tempOldCurrentFolder);
    }
    files()->rename($tempCurrentFolder, get('deploy_path') . '/current');
    writeln('done.');

    $releasesJsonFilePath = get('deploy_path') . '/.dep/releases.json';
    $releases = json_decode(files()->read($releasesJsonFilePath), true) ?: [];
    foreach ($releases as $index => $release) {
        if ($release['name'] === get('release_name')) {
            $releases[$index]['current'] = true;
        } else {
            if (isset($release['current'])) {
                unset($releases[$index]['current']);
            }
        }
    }

    if ($tempOldCurrentFolder) {
        writeAndLog('Delete old current directory... ');
        files()->deleteDir($tempOldCurrentFolder);
        writeln('done.');
    }

    files()->write($releasesJsonFilePath, json_encode($releases, JSON_PRETTY_PRINT));
    writelnAndLog('Updated current release in ' . $releasesJsonFilePath);
});
