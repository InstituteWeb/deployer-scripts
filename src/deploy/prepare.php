<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\cd;
use function Deployer\desc;
use function Deployer\get;
use function Deployer\run;
use function Deployer\task;
use League\Flysystem\FileNotFoundException;

desc('Prepares environment on remote server, and create deployment folder structure within "deploy_path".');
task('deploy:prepare', function() {
    if (!get('revision')) {
        throw new \RuntimeException(
            'Deploy scripts require set --revision option or add the git revision as first argument. ' .
            'Or execute build tasks before, then revision option is set automatically (in build:update_code).'
        );
    }

    if (!files()->has(get('deploy_path'))) {
        files()->createDir(get('deploy_path'));
        writelnAndLog('Created directory "' . get('deploy_path') . '" on remote.');
    }
    cd('');

    $result = explode("\n", run(get('bin/php') . ' --version')->toString());
    $phpVersion = array_shift($result);
    writelnAndLog('PHP version (from cli binary) on remote: ' . $phpVersion);

    /**
     * Creates directory on remote and logs/outputs process
     *
     * @param string $directory
     * @return bool
     */
    $makeDirectoryOnRemote = function($directory) {
        if (!files()->has($directory)) {
            $status = files()->createDir($directory);
            if ($status) {
                writelnAndLog('Created directory "' . $directory . '"');
                return true;
            } else {
                throw new \RuntimeException('Can\'t create directory "' . $directory . '"');
            }
        }
        return false;
    };

    // Create directories
    $makeDirectoryOnRemote(get('deploy_path') . '/releases');
    $makeDirectoryOnRemote(get('deploy_path') . '/shared');
    $makeDirectoryOnRemote(get('deploy_path') . '/.dep');

    // Create .dep/releases.json file with empty array content if not existing
    $releasesFile = get('deploy_path') . '/.dep/releases.json';
    try {
        files()->get($releasesFile);
    } catch (FileNotFoundException $e) {
        files()->write($releasesFile, '{}');
        writelnAndLog('Created empty releases.json file (important for rollbacks).');
    }

});
