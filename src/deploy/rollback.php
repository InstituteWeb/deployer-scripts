<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\after;
use function Deployer\cd;
use function Deployer\desc;
use function Deployer\get;
use function Deployer\input;
use function Deployer\option;
use function Deployer\set;
use function Deployer\task;

option('to', null, \Symfony\Component\Console\Input\InputOption::VALUE_OPTIONAL, 'Name of release to rollback. If empty the previous release is taken.');

desc('Rollbacks to previous (or given) release');
task('rollback', [
    'deploy:rollback',
    'deploy:rollout'
]);
after('rollback', 'deploy:typo3:run_post_update_cmd');

desc('Returns to a previous release on remote, if available.');
task('deploy:rollback', function() {
    cd('');

    $releasesJsonFilePath = get('deploy_path') . '/.dep/releases.json';
    $releases = json_decode(files()->read($releasesJsonFilePath), true);
    if (!$releases) {
        throw new \RuntimeException('Unable to get releases from "' . $releasesJsonFilePath . '" file.');
    }

    $overrideRelease = null;
    $previousRelease = null;
    if (input()->hasOption('to') && !empty(input()->getOption('to'))) {
        if (input()->getOption('to') === 'latest') {
            $overrideRelease = end($releases);
        } else {
            foreach ($releases as $release) {
                if (input()->getOption('to') === $release['name'] && files()->has($release['path'])) {
                    $overrideRelease = $release;
                    break;
                }
            }
        }
        if (is_null($overrideRelease)) {
            throw new \RuntimeException('Given release "' . input()->getOption('to') . '" not found or it is missing in filesystem.');
        }

        $previousRelease = $overrideRelease;
        writeln('Roll back to "' . $previousRelease['name'] . '"...');
    }

    if (is_null($overrideRelease)) {
        $currentRelease = null;
        foreach ($releases as $release) {
            if (isset($release['current']) && $release['current'] !== false) {
                $currentRelease = $release;
                break;
            }
            if (files()->has($release['path'])) {
                $previousRelease = $release;
            } else {
                writelnAndLog('Found missing release in releases.json: ' . $release['path'] . ' not found!', \Monolog\Logger::WARNING);
            }
        }
        if (is_null($currentRelease)) {
            throw new \RuntimeException(
                'No current release defined. Use --to to specify release to roll back to.'
            );
        }
        if (is_null($previousRelease)) {
            throw new \RuntimeException('Unable to find release to rollback to. Please speficy name of release to roll back to using --to option.');
        }
        writeln('Rolling back from "' . $currentRelease['name'] . '" to "' . $previousRelease['name'] . '"...');
    }

    set('release_name', $previousRelease['name']);
})->setPrivate();

