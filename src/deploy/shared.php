<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\cd;
use function Deployer\desc;
use function Deployer\get;
use function Deployer\logger;
use function Deployer\run;
use function Deployer\task;

desc('Copies shared dirs and files from releases to shared folder and replace the originals with symlinks.');
task('deploy:shared', function() {
    cd('');
    $sharedPath = get('deploy_path') . '/shared';

    if (get('shared_dirs') && count(get('shared_dirs')) > 0) {
        writeln('Shared dirs:');
    }
    foreach (get('shared_dirs') as $dir) {
        writeln($sharedPath . '/' . $dir);

        // Create shared folders if not existing
        if (files()->has($sharedPath . '/' . $dir)) {
            files()->createDir($sharedPath . '/' . $dir);
            logger('Created directory "' . $sharedPath . '/' . $dir . '".');
            writeln('- directory missing, created');
        }

        // If release contains shared dirs, copy and delete them
        if (files()->has(get('release_path') . '/' . $dir)) {
            write('- copying files from release to shared dir... ');
            $destination = $sharedPath . '/' . dirname($dir);
            if (!files()->has($destination)) {
                files()->createDir($destination);
            }
            logger('Copy"' . get('release_path') . '/' . $dir . '" to "' . $destination . '"...');
            files()->copy(get('release_path') . '/' . $dir, $destination);
            writeln('done');
            write('- deleting "' . $dir . '" in release... ');
            files()->delete(get('release_path') . '/' . $dir);
            writeln('done');
        }

        // Symlink shared dir in release to new location
        run(vsprintf(get('bin/symlink'), [$sharedPath . '/' . $dir, get('release_path') . '/' . $dir]));
        writeln('- created symlink for "' . $dir . '" in release');
        logger('Created symlink "' . get('release_path') . '/' . $dir . '" pointing to "' . $sharedPath . '/' . $dir . '"');
    }

    if (get('shared_files') && count(get('shared_files')) > 0) {
        writeln('Shared files:');
    }
    foreach (get('shared_files') as $file) {
        writeln($sharedPath . '/' . $file);

        if (!files()->has($sharedPath . '/' . $file)) {
            files()->write($sharedPath . '/' . $file, '');
            writeln('- file missing, created');
            logger('Created shared file: ' . $sharedPath . '/' . $file);
        }

        if (files()->has(get('release_path') . '/' . $file)) {
            files()->delete(get('release_path') . '/' . $file);
            writeln('- deleted shared file in release');
            logger('Deleted shared file in release: ' . get('release_path') . '/' . $file);
        }

        // Symlink shared file in release
        run(vsprintf(get('bin/symlink-file'), [$sharedPath . '/' . $file, get('release_path') . '/' . $file]));
        writeln('- created symlink for "' . $file . '" in release');
        logger('Created symlink "' . get('release_path') . '/' . $file . '" pointing to "' . $sharedPath . '/' . $file . '"');
    }
});
