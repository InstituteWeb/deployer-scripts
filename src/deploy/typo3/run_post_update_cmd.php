<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\cd;
use function Deployer\desc;
use function Deployer\get;
use function Deployer\logger;
use function Deployer\run;
use function Deployer\task;
use function Deployer\writeln;

desc('Updates database schemas using composer\'s post-update-cmd scripts.');
task('deploy:typo3:run_post_update_cmd', function() {
    cd('');

    $composerCall = get('bin/composer') . ' run-script post-update-cmd -n --working-dir "' . get('deploy_path') . '/current' . '"';
    writeAndLog('Performing "' . $composerCall . '" on remote... ');

    $result = run($composerCall);
    writeln('done.');
    logger('Composer ' . $composerCall . ' done.');
    logger('Composer Output: ' . PHP_EOL . $result->getOutput());
});
