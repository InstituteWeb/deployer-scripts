<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\cd;
use function Deployer\desc;
use function Deployer\get;
use function Deployer\input;
use function Deployer\logger;
use function Deployer\task;

desc('Locks deployer and creates /.dep/.lock file');
task('deploy:lock', function() {
    cd('');

    if (files()->has(get('deploy_path') . '/.dep/.lock')) {
        if (input()->hasOption('force') && input()->getOption('force')) {
            writelnAndLog('Force mode set. Ignoring existing "' . get('deploy_path') . '/.dep/.lock" file.');
        } else {
            $contents = files()->read(get('deploy_path') . '/.dep/.lock');
            throw new \RuntimeException(
                'Deployment already locked (file "' . get('deploy_path') . '/.dep/.lock' . '" existing)' . PHP_EOL . $contents
            );
        }
    }

    files()->write(get('deploy_path') . '/.dep/.lock', 'since ' . date('r'));
    logger('Deployer locked.');
});
