<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\desc;
use function Deployer\get;
use function Deployer\parse;
use function Deployer\run;
use function Deployer\task;
use function Deployer\upload;

desc('Uploads file(s) to desired and configured server. If it is an zip archive it is extracted in folder named like zip file and deleted afterwards.');
task('deploy:upload', function() {
    $uploadList = get('upload_list');
    if (!$uploadList || empty($uploadList) || !is_array($uploadList) ) {
        throw new \RuntimeException('Given "upload_list" is invalid. Array with key (local file) and values (remote destination) expected.');
    }
    foreach ($uploadList as $from => $to) {
        $from = realpath(parse($from));
        $to = parse($to);

        if (!$from) {
            throw new \RuntimeException('Can\'t upload "' . parse($from) . '" ("' . $from . '"). Not found on local machine.');
        }

        upload($from, $to);

        // extract if uploaded file is zip archive
        if (substr($from, -4) === '.zip') {
            $extractTo = substr($to, 0, -4);
            if (files()->has($extractTo)) {
                writeAndLog('Directory to extract to already existing. Deleting... ');
                files()->deleteDir($extractTo);
                writeln('done.');
            }
            writeAndLog('Extracting file "' . $to . '" to "' . $extractTo . '"... ');

            $command = vsprintf(get('bin/unzip'), ['zipPath' => $to, 'destination' => $extractTo]);
            run($command);
            writeln('done.');

            $deleteStatus = files()->delete($to);
            if ($deleteStatus) {
                writelnAndLog('Removed zip file "' . $to . '" from remote.');
            } else {
                writelnAndLog('Unable to delete zip file "' . $to . '" from remote! File remains in place.', \Monolog\Logger::WARNING);
            }
        }
    }
});
