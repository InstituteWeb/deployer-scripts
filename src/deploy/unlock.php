<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\cd;
use function Deployer\desc;
use function Deployer\get;
use function Deployer\logger;
use function Deployer\task;

desc('Removes /.dep/.lock file and unlocks deployer.');
task('deploy:unlock', function() {
    cd('');

    if (files()->has(get('deploy_path') . '/.dep/.lock')) {
        files()->delete(get('deploy_path') . '/.dep/.lock');
        logger('Deployer unlocked.');
    }
});
