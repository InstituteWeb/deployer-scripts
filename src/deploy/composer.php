<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\cd;
use function Deployer\desc;
use function Deployer\get;
use function Deployer\logger;
use function Deployer\run;
use function Deployer\task;

desc('Runs composer on remote.');
task('deploy:composer', function() {
    cd('');

    $composerCall = get('bin/composer') . ' ' . get('composer_options') . ' --working-dir "' . get('release_path') . '"';
    writeAndLog('Performing "' . $composerCall . '" on remote... ');

    $result = run($composerCall);
    writeln('done.');
    logger('Composer ' . get('composer_action') . ' done.');
    logger('Composer Output: ' . PHP_EOL . $result->getOutput());
});
