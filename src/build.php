<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\after;
use function Deployer\before;
use function Deployer\desc;
use function Deployer\get;
use function Deployer\parse;
use function Deployer\task;
use function Deployer\writeln;


// Tasks
desc('Build your TYPO3 project (no frontend, requires git and composer available)');
task('build', [
    'build:create',
    'build:update_code',
//    'build:frontend',
    'build:archive',
    'build:clear'
]);
before('build', 'build-start');
after('build', 'build-successful');

desc('Build your TYPO3 project from local git. Be sure to have the correct files checked out!!');
task('build-no-remote', [
    'build:create',
    'build:copy_staged_files',
    'build:composer',
    'build:archive',
    'build:clear'
]);
before('build-no-remote', 'build-start');
after('build-no-remote', 'build-successful');


desc('Full build of your TYPO3 project (including frontend, which requires nodejs and npm available)');
task('build-full', [
    // ...
    'build'
]);

/**
 * Start message
 */
task('build-start', function () {
    writelnAndLog('Start with building...', \Monolog\Logger::INFO);
})->once()->setPrivate();

/**
 * Success message
 */
task('build-successful', function () {
    writelnAndLog(parse('Build "{{build_name}}" successful.'), \Monolog\Logger::INFO);
})->once()->setPrivate();

desc('Displays list of available builds.');
task('list-builds', function() {
    writeln('Found builds in "' . get('build_directory') . '":');
    $files = filesLocal()->listContents(get('build_directory'));
    if (count($files) === 0) {
        writeln('none.');
    } else {
        foreach ($files as $file) {
            if ('.zip' === substr($file['path'], -4)) {
                writeln('- ' . substr(basename($file['path']), 0, -4) . ' (' . date('d.m.Y H:i:s', filemtime($file['path'])) . ')');
            }
        }
        writeln('');
    }
});
