<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use SplFileInfo;

/**
 * Class LocalAdapter
 *
 * @package InstituteWeb\DeployerScripts
 */
class LocalAdapter extends \League\Flysystem\Adapter\Local
{
    /**
     * Get the normalized path from a SplFileInfo object.
     *
     * @param SplFileInfo $file
     * @return string
     */
    protected function getFilePath(SplFileInfo $file)
    {
        $location = $file->getPathname();

        // Do not remove path prefix on windows
        if ('\\' === DIRECTORY_SEPARATOR) {
            return $location;
        }
        $path = $this->removePathPrefix($location);
        return trim(str_replace('\\', '/', $path), '/');
    }


    /**
     * @inheritdoc
     * @see https://github.com/thephpleague/flysystem/pull/740
     */
    public function applyPathPrefix($path)
    {
        if ('\\' === DIRECTORY_SEPARATOR) {
            return $path;
        }
        $prefixedPath = parent::applyPathPrefix($path);

        return str_replace('/', DIRECTORY_SEPARATOR, $prefixedPath);
    }

    /**
     * @inheritdoc
     */
    public function delete($path)
    {
        if (DIRECTORY_SEPARATOR === '\\') {
            // if is windows
            $cmd = 'del';
            if (is_dir($path)) {
                $cmd = 'rmdir';
            }
            $process = new \Symfony\Component\Process\Process($cmd . ' /S /Q "' . $path . '"');
            $process->run();
            sleep(0.5);
            return $process->isSuccessful();
        }
        return parent::delete($path);
    }

    public function deleteDir($dirname)
    {
        if (DIRECTORY_SEPARATOR === '\\') {
            return $this->delete($dirname);
        }
        return parent::deleteDir($dirname);
    }

    /**
     * Runs command on local using deployer
     *
     * @param string $cmd
     * @return \Deployer\Type\Result
     */
    public function run($cmd)
    {
        return \Deployer\runLocally($cmd);
    }
}
