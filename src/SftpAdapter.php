<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Class SftpAdapter
 *
 * @package InstituteWeb\DeployerScripts
 */
class SftpAdapter extends \League\Flysystem\Sftp\SftpAdapter
{
    /**
     * Using "rm -rf" on cli to remove directory/file
     *
     * @param string $dirname
     * @return \Deployer\Type\Result
     */
    public function deleteDir($dirname)
    {
        return $this->run(vsprintf(\Deployer\get('bin/rm'), [$dirname]));
    }

    /**
     * Using "cp -rf" on cli to copy directory/file
     * @param string $path
     * @param string $newpath
     * @return \Deployer\Type\Result
     */
    public function copy($path, $newpath)
    {
        return $this->run(vsprintf(\Deployer\get('bin/cp'), [$path, $newpath]));
    }

    /**
     * Runs command on remote using deployer's connection
     *
     * @param string $cmd
     * @return \Deployer\Type\Result
     */
    public function run($cmd)
    {
        return \Deployer\run($cmd);
    }
}
