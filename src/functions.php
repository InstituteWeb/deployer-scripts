<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use function Deployer\get;
use function Deployer\input;
use function Deployer\logger;


/**
 * Writes message to new line (if level is INFO or higher, or verbose option is set)
 *
 * @param string $message
 * @param int $level
 */
function writeln($message, $level = \Monolog\Logger::DEBUG)
{
    if ($level > \Monolog\Logger::DEBUG || input()->getOption('verbose')) {
        \Deployer\writeln($message);
    }
}

/**
 * Writes message without line ending  (if level is INFO or higher, or verbose option is set)
 *
 * @param string $message
 * @param int $level
 */
function write($message, $level = \Monolog\Logger::DEBUG)
{
    if ($level > \Monolog\Logger::DEBUG || input()->getOption('verbose')) {
        \Deployer\write($message);
    }
}


/**
 * Writes message (with new line) to output and log  (if level is INFO or higher, or verbose option is set)
 *
 * @param string|array $message
 * @param int $level
 * @return void
 */
function writelnAndLog($message, $level = \Monolog\Logger::DEBUG)
{
    writeln($message, $level);
    if (is_string($message)) {
        logger($message, $level);
    }
}

/**
 * Writes message to output and log  (if level is INFO or higher, or verbose option is set)
 *
 * @param string|array $message
 * @param int $level
 * @return void
 */
function writeAndLog($message, $level = \Monolog\Logger::DEBUG)
{
    write($message, $level);
    if (is_string($message)) {
        logger($message, $level);
    }
}

/**
 * Filesystem for remote server (SSH/SFTP)
 *
 * @return Filesystem
 */
function files()
{
    $server = \Deployer\Task\Context::get()->getServer();
    if (!isset($server->_filesystem)) {
        $adapter = new SftpAdapter([
            'host' => $server->getConfiguration()->getHost(),
            'port' => $server->getConfiguration()->getPort(),
            'username' => $server->getConfiguration()->getUser(),
            'password' => $server->getConfiguration()->getPassword(),
            'privateKey' => $server->getConfiguration()->getPrivateKey(),
            'timeout' => 10,
            'root' => '/'
        ]);
        $adapter->setDirectoryPerm(get('directory_permissions'));
        $server->_filesystem = new Filesystem($adapter, ['disable_asserts' => true]);

    }
    return $server->_filesystem;
}

/**
 * Filesystem of local machine
 *
 * @return Filesystem
 */
function filesLocal()
{
    $rootPath = getcwd();
    do {
        $rootPath = dirname($rootPath);
    } while (dirname($rootPath) !== $rootPath);
    return new Filesystem(new LocalAdapter($rootPath));
}
