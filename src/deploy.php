<?php
namespace InstituteWeb\DeployerScripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use function Deployer\desc;
use function Deployer\get;
use function Deployer\parse;
use function Deployer\task;
use function Deployer\writeln;


desc('Deploys your local build to remote servers.');
task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:upload',
    'deploy:composer',
    'deploy:shared',
    'deploy:rollout',
    'deploy:typo3:run_post_update_cmd',
    'deploy:unlock',
]);

/**
 * Start message
 */
task('deploy-start', function () {
    writelnAndLog('Start with deployment...', \Monolog\Logger::INFO);
})->once()->setPrivate();

/**
 * Success message
 */
task('deploy-successful', function () {
    writelnAndLog(parse('Deployment to "{{deploy_path}}" successful.'), \Monolog\Logger::INFO);
})->once()->setPrivate();


desc('Displays list of release on given remote.');
task('list-releases', function() {
    $releases = files()->read(get('deploy_path') . '/.dep/releases.json');
    if (!$releases) {
        throw new \RuntimeException('File "' . get('deploy_path') . '/.dep/releases.json' . '" not existing. No release found.');
    }

    writeln('Max release cycle count: '  . get('max_release_cycle'));
    writeln('Found releases on "' . \Deployer\Task\Context::get()->getServer()->getConfiguration()->getName() . '":');
    writeln('');
    $releases = json_decode($releases, true);
    if (!$releases) {
        writeln('none.');
    }

    foreach ($releases as $index => $release) {
        $missing = '';
        if (!files()->has($release['path'])) {
            $missing = ' <error> missing! </error>';
        }
        $currentLabel = '';
        if (isset($release['current']) && $release['current'] === true) {
            $currentLabel = ' <info>+++ CURRENT RELEASE! +++</info>';
        }
        writeln([
            '#' . $index . $currentLabel,
            'Name: ' . $release['name'],
            'Path: ' . $release['path'] . $missing,
            'Date: ' . $release['time'],
            ''
        ]);
    }
});
